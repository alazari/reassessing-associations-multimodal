function finger_tapping
% "finger tapping task" using Matlab and PsychoToolbox

% version log
% 2017-09-21    Alberto writes original code based on description from  Sullivan 2001,
% Muetzel 2008, Pelletier, 1993; and suggestions from Mel Fleming
%
% This file is part of the Oxford quantitative MRI behavioural experiment
% Alberto Lazari      alberto.lazari@ndcn.ox.ac.uk
% version 2017-09-21



%% INITIALIZE
%====================

clear all;

%set up structures 
cfg = [];
cfg.n.cond = 3;
cfg.n.repeats = 4;
cfg.n.maxpress = 30;
cfg.n.presses= cfg.n.cond*cfg.n.repeats*cfg.n.maxpress;
cfg.h.keyboard = [];

tim = [];
tim.response = [];

log =[];

h_keyboard = [];

previous_key = [];

%get Subject ID
cfg.subID=strjoin(inputdlg('Subject ID'));

% synchronize clock with screen, and get offset?
tim.start = GetSecs;
log.tim.offset = tim.start;

% set up keys
cfg.key.left = KbName('a');
cfg.key.right = KbName('l');
cfg.key.escape = KbName('DELETE');

%hide cursor
HideCursor;

% definitions and settings (boolean flags)
cfg.flg.debug = 1;          % run in debug mode (1) or not (0)
cfg.flg.suppresschecks = 2; % supress all PsychToolbox quality checks (2), just the sync testing (1), or none (0)
cfg.flg.dontclear = 1;      % keep stimuli in bugger after `flip` until instructed otherwise
cfg.flg.primaryscreen = 0;  % use the primary screen to present the stimuli (1) or not (0)

% initialize settings, input/output, display, conditions, logfile
pref = [];
if cfg.flg.suppresschecks > 0
  % change the testing parameters
  %default: Screen('Preference','SyncTestSettings' [, maxStddev=0.001 secs][, minSamples=50][, maxDeviation=0.1][, maxDuration=5 secs]);
  [p1, p2, p3, p4] = Screen('Preference','SyncTestSettings',0.001,50,0.1,5);
  pref.old.SyncTestSettings = num2cell([p1 p2 p3 p4]);
  if cfg.flg.suppresschecks > 1
    % and suppress them completely
    pref.old.SkipSyncTests = Screen('Preference','SkipSyncTests',1);
    pref.old.VisualDebugLevel = Screen('Preference','VisualDebugLevel',3);
    pref.old.SupressAllWarnings = Screen('Preference','SuppressAllWarnings',1);
  end
end

Screen('Preference', 'VisualDebuglevel', 3);
Screen('Preference','SkipSyncTests', 1);

%%setup display
% get screen handles
cfg.h.allscreens = Screen('Screens');

% use primary or secondary screen to present stimuli
if cfg.flg.primaryscreen
  % use primary display
  if ispc
    cfg.h.screen = min(max(cfg.h.allscreens),1);
  else
    cfg.h.screen = min(cfg.h.allscreens);
  end
else
  % use secondary display
  cfg.h.screen = max(cfg.h.allscreens);
end

% define colors
cfg.colour.black = BlackIndex(cfg.h.screen);
cfg.colour.white = WhiteIndex(cfg.h.screen);
cfg.colour.gray = (cfg.colour.black+cfg.colour.white)/2;
cfg.colour.red = [1 0 0]';
cfg.colour.green = [0 1 0]';
cfg.colour.blue = [0 0 1]';

% initialize the size of the display window
if cfg.flg.debug
  cfg.pos.win = [0 0 640 480];
else
  cfg.pos.win = [];
end

% open a display window, get the window handle and size
[cfg.h.window, cfg.pos.win] = PsychImaging('OpenWindow', cfg.h.screen, cfg.colour.black, cfg.pos.win, 32, 2);

% retrieve the size of the display window
[cfg.pos.width, cfg.pos.height] = Screen('WindowSize', cfg.h.window);
[cfg.pos.xCentre, cfg.pos.yCentre] = RectCenter(cfg.pos.win);

% switch to realtime-priority to reduce timing jitter and interruptions
% caused by other applications and the operating system itself:
if ispc
  Priority(0);
  %Priority(1); % This is not real-time priority to give the screen-capture program a chance to run smoothly
else
  Priority(MaxPriority(cfg.h.window));
end

% get the flip interval (time between frame refresh)
cfg.dur = [];
cfg.dur.frame = Screen('GetFlipInterval',cfg.h.window);
% calculate a buffer of half a frame rate
cfg.dur.buffer = cfg.dur.frame/2;

% always perform CleanUp function, even after an error
obj_cleanup = onCleanup(@() CleanUp(cfg.h,pref));

% start a keyboard queue to record the key presses and releases
%cfg.h = StartKeyBoardQueue(cfg.h);

%initialise log
[cfg, log] = InitLog(cfg);
LogHeader(tim, log);

log.data = zeros(cfg.n.presses,length(log.varName));


%% MAIN TASK LOOP
%%%%%%%%%%%%%%%%%%%

%go over each repeat
for x = 1: cfg.n.repeats
    
% go over each task condition
for t = 1:cfg.n.cond
    
%present instructions
PresentInstructions(x, t, cfg);

%present fixation cross
PresentCross(cfg);
tim.cross=GetSecs;

%count to 30 keypresses under a black screen

for press= 1:cfg.n.maxpress
    
KbQueueCreate(h_keyboard);
KbQueueStart(h_keyboard);

escapeTime = GetSecs + 10000;

if t ==1
cfg.key.response = [cfg.key.right];    
end

if t ==2
cfg.key.response = [cfg.key.left];
end

if t ==3
cfg.key.response = [cfg.key.left cfg.key.right];
end

next=true;

while next
[tim.response.press, key] = WaitResponse(cfg.h.keyboard, cfg.key.response, 1, escapeTime, cfg.key.escape, 1);

if key == cfg.key.right
[tim.response.release, key] = WaitResponse(cfg.h.keyboard, cfg.key.right, 0, escapeTime, cfg.key.escape, 1);
end

if key == cfg.key.left
[tim.response.release, key] = WaitResponse(cfg.h.keyboard, cfg.key.left, 0, escapeTime, cfg.key.escape, 1);
end

next=false;

end

press = press-1;

[log] = LogPress(log, tim, x, t, key, press);

previous_key = key;

KbQueueStop(h_keyboard);
KbQueueRelease(h_keyboard);

end

%present feedback
PresentFeedback(log, x, t, cfg);

end
end
    
% end of experiment screen. We clear the screen once they have made their response
DrawFormattedText(cfg.h.window, 'Thank you!\nThe task is finished.\n\nPress Esc Key To Exit.', 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);

% Wait for a key press
KbStrokeWait;

% Clear the screen
sca;

end

%====================
%% TASK FUNCTIONS
%====================

function PresentInstructions(x, t, cfg)
%%Present instructions
%--------------------

if t == 1
    
if x == 1
% welcome to the task
messageStr = ' Welcome to the Finger Tapping task!';
DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);
KbStrokeWait;

% explaining the finger tapping
messageStr = 'We will ask you to tap your finger 30 times on the button, as fast as you can';
DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);
KbStrokeWait;

% explaining the conditions
messageStr = 'You will do this three times - each time with different fingers';
DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);
KbStrokeWait;

% explaining the counting
messageStr = 'The computer will notify you when you''ve reached 30 finger presses \n\n no need to count yourself ';
DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);
KbStrokeWait;
end

% instruct right index finger
messageStr = sprintf('Part \t%f: Please press the key with your right index finger only, as fast as you can \n\n Press any key to start', t+((x-1)*4));

end

if t == 2
% Part 1 is over
messageStr = 'End of Part 1!';

DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);

%let participant cool down
WaitSecs(2);

KbStrokeWait;

% explaining left index finger
messageStr = 'Part 2: Please press the key with your left index finger only, as fast as you can \n\n Press any key to start';
  
end

if t == 3
% Part 1 is over
messageStr = 'End of Part 2!';

DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);

%let participant cool down
WaitSecs(2);

KbStrokeWait;  

% explaining left index finger
messageStr = 'Part 3: Please press the key alternating your right and left index fingers, as fast as you can \n\n Press any key to start';
end

%presenting the message string
DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);
KbStrokeWait;
    
end

function [tim, key, previousPress, previousRelease] = WaitResponse(h,key,flgPressRelease,escapeTime,escapeKey,flgResetQueue)
%--------------------------------------------------------------------------
% WaitPress: wait for the press or release of a specified key. Please
% create and start a cue before calling this function and stop and close
% the queue afterwards.
%
% KbQueueCreate(h_keyboard);
% KbQueueStart(h_keyboard);
% (do your magic here)
% KbQueueStop(h_keyboard);
% KbQueueRelease(h_keyboard);
%
%
% INPUT
% key           - any (set of) key(s) on the keyboard, for example: KbName('1!')
% flgPressRelease  - 1 to detect key press, 0 for release
% escapeTime   - always return after this (absolute time)
% escapeKey 	- always return if this key is pressed. For example: escapeKey = KbName('ESCAPE')
% h_keyboard    - deviceIndex of the keyboard
%
% OUTPUT
% tim           - time of event (press, release, or escape)
% key           - key number, or 0 for time, or -1 for escape key
%
% Copyright (C) 2013-2017, Lennart Verhagen
% lennart.verhagen@psy.ox.ac.uk
% version 2017-08-22
%--------------------------------------------------------------------------

end

function LogHeader(tim, log)
%% LogHeader
%----------
% write header to file
fileID = fopen(log.fileName.header,'a');
if exist(log.fileName.header,'file')==2, fprintf(fileID,'\n\n'); end
fprintf(fileID,'----------------------------------------\n');
fprintf(fileID,'MYELIN and PLASTICITY\n');
fprintf(fileID,'\tFinger Tapping Task\n');
fprintf(fileID,'\tquantitative Magnetic Resonance Imaging\n');
fprintf(fileID,'\tAlberto Lazari, Olof van der Werf, and Lennart Verhagen\n\n');
fprintf(fileID,'date:\t\t%s\n',datestr(now));
fprintf(fileID,'subject:\t%s\n',log.subjectName);
fprintf(fileID,'timeStart:\t%.6f\n\n',tim.start);
fprintf(fileID,'variables:\n');
fprintf(fileID,'\t%s\n',log.varName{:});
fprintf(fileID,'----------------------------------------\n');
fclose(fileID);
end

function [log] = LogSet(log, x, t, varName, dat)
%% LogSet
%----------
idx = ismember(log.varName,varName);
if ~any(idx), warning('paTMS:log:varNameUnknown','variable name ''%s'' not recognized',varName); end

% adjust timing if needed
if ~isempty(regexp(varName,log.tim.varIdentifier,'once')) == 1 && dat > 0
    dat = dat - log.tim.offset;
end

% store data in logfile matrix
log.data(x,idx) = dat;
end

function [log] = LogPress(log, tim, x, t, key, press);
%% LogPress
%----------
% calculates the press and release times and logs them in the log structure

crossTime = tim.cross - tim.start;
pressRT = tim.response.press - tim.cross;
releaseRT = tim.response.release - tim.cross;

% store the response in the log
log = LogSet(log, (press+1)+(30*(t-1))+(90*(x-1)), (press+1)+(30*(t-1)), 'RepeatNumber', x);
log = LogSet(log, (press+1)+(30*(t-1))+(90*(x-1)), (press+1)+(30*(t-1)), 'CondNumber', t);
log = LogSet(log, (press+1)+(30*(t-1))+(90*(x-1)), (press+1)+(30*(t-1)), 'CrossTime', crossTime);
log = LogSet(log, (press+1)+(30*(t-1))+(90*(x-1)), (press+1)+(30*(t-1)), 'pressNumber', press+1);
log = LogSet(log, (press+1)+(30*(t-1))+(90*(x-1)), (press+1)+(30*(t-1)), 'PressRT', pressRT);
log = LogSet(log, (press+1)+(30*(t-1))+(90*(x-1)), (press+1)+(30*(t-1)), 'ReleaseRT', releaseRT);
log = LogSet(log, (press+1)+(30*(t-1))+(90*(x-1)), (press+1)+(30*(t-1)), 'Key', key);

%write data to log
LogWrite(log, x, (press+1)+(30*(t-1))+(90*(x-1)));

end

function LogWrite(log, x, t)
%% LogWrite
%----------

% exctract the trial data from the log
dat = log.data(t,:);

% create a format string that matches the logfile data type
formatStr = repmat({'%.6f\t'},1,length(log.varName));     % by default all data are fixed-point numbers
formatStr(rem(dat,1)==0) = {'%d\t'};                      % replace integers
formatStr = regexprep(strcat(formatStr{:}),'\\t$','\\n'); % concatenate and replace last tab by a new-line

% write the logfile to the disk
fileID = fopen(log.fileName.data,'a');
fprintf(fileID,formatStr,log.data(t,:));
fclose(fileID);
end

function PresentFeedback(log, x, t, cfg)
RTestimate= log.data(30+(30*(t-1))+(90*(x-1)),6) - log.data(1+(30*(t-1))+(90*(x-1)),6);
messageStr = sprintf('Total time for 30 presses: \t%.6f seconds', RTestimate);

DrawFormattedText(cfg.h.window, messageStr, 'center', 'center', cfg.colour.white);
Screen('Flip', cfg.h.window);

%let participant cool down
WaitSecs(2);

KbStrokeWait;

end

function CleanUp(h, pref)
%% CleanUp
%----------
%this function does a few important things if the task crashes:

if nargin < 1, h.keyboard = []; end
if nargin < 2, pref = []; end

% enable keyboard for Matlab
ListenChar(0);
% close and release cue
KbQueueStop(h.keyboard);
KbQueueRelease(h.keyboard);

% shutdown realtime scheduling
Priority(0);

% close window and restore cursor and other settings
sca;

% restore preferences - does this need to be deleted?
if ~isempty(pref)
  if isfield(pref.old,'SyncTestSettings')
    Screen('Preference','SyncTestSettings',pref.old.SyncTestSettings{:});
  end
  if isfield(pref.old,'SkipSyncTests')
    Screen('Preference','SkipSyncTests',pref.old.SkipSyncTests);
    Screen('Preference','VisualDebugLevel',pref.old.VisualDebugLevel);
    Screen('Preference','SuppressAllWarnings',pref.old.SupressAllWarnings);
  end
  
end
end
