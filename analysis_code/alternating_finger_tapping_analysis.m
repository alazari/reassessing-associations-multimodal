% -----------------------------------
% GROUP-LEVEL ANALYSIS OF AFT DATA
% -----------------------------------

subjects = {'PP03','PP04','PP05','PP08','PP09','PP10','PP11','PP12','PP13','PP14','PP15','PP16','PP17','PP18','PP19','PP20','PP21','PP22','PB01','PB02','PB03','PB04','PB05','PB06','PB07','PB08','PB09','PB10','PB11','PB12','PB14','PB15','PB16','PB17','PB18','PB19','PB20','PB21','PB22','PB23','PB24','PB25','PB26','PB27','PB28','PB29','PB30', 'PB31', 'PB32'};
samplesize=length(subjects);
groupData = [];


for j = 1:samplesize
    
% Fetch responses 
cfg.dir = fullfile(filesep, 'Finger_tapping_data');
cfg.subject = char(subjects(j));
cfg.filename = ['/logfile_data_',cfg.subject,'.txt'];
responses = load([cfg.dir,cfg.filename]);

% create an empty matrix for Reaction Times
RT = [];
responseblocks = [];

% extract right finger tapping parameters
rightDuration1 = responses(responses(:, 2)==1 & responses(:, 1)==2 & responses(:, 4)==30, 5) - responses(responses(:, 2)==1 & responses(:, 1)==2 & responses(:, 4)==1, 5);
rightDuration2 = responses(responses(:, 2)==1 & responses(:, 1)==3 & responses(:, 4)==30, 5) - responses(responses(:, 2)==1 & responses(:, 1)==2 & responses(:, 4)==1, 5);
rightDuration3 = responses(responses(:, 2)==1 & responses(:, 1)==4 & responses(:, 4)==30, 5) - responses(responses(:, 2)==1 & responses(:, 1)==2 & responses(:, 4)==1, 5);
rightDurationMean = mean([rightDuration1 rightDuration2 rightDuration3]);

% extract left finger tapping parameters
leftDuration1 = responses(responses(:, 2)==2 & responses(:, 1)==2 & responses(:, 4)==30, 5) - responses(responses(:, 2)==2 & responses(:, 1)==2 & responses(:, 4)==1, 5);
leftDuration2 = responses(responses(:, 2)==2 & responses(:, 1)==3 & responses(:, 4)==30, 5) - responses(responses(:, 2)==2 & responses(:, 1)==2 & responses(:, 4)==1, 5);
leftDuration3 = responses(responses(:, 2)==2 & responses(:, 1)==4 & responses(:, 4)==30, 5) - responses(responses(:, 2)==2 & responses(:, 1)==2 & responses(:, 4)==1, 5);
leftDurationMean = mean([leftDuration1 leftDuration2 leftDuration3]);

% extract alternating finger tapping parameters
alternatingDuration1 = responses(responses(:, 2)==3 & responses(:, 1)==2 & responses(:, 4)==30, 5) - responses(responses(:, 2)==3 & responses(:, 1)==2 & responses(:, 4)==1, 5);
alternatingDuration2 = responses(responses(:, 2)==3 & responses(:, 1)==3 & responses(:, 4)==30, 5) - responses(responses(:, 2)==3 & responses(:, 1)==2 & responses(:, 4)==1, 5);
alternatingDuration3 = responses(responses(:, 2)==3 & responses(:, 1)==4 & responses(:, 4)==30, 5) - responses(responses(:, 2)==3 & responses(:, 1)==2 & responses(:, 4)==1, 5);
alternatingDurationMean = median([alternatingDuration1 alternatingDuration2 alternatingDuration3]);

% extract normalised finger tapping parameter
normAlternating = alternatingDurationMean / mean([rightDurationMean leftDurationMean]);

RT=responses(:, 5);
for i = 2:length(RT)
    RT(i-1)=RT(i)-RT(i-1);
end 
RT(length(RT))=[];

% extract variability parameters
rightfingerRTs = [RT((1+90):(29+90))' RT((1+180):(29+180))' RT((1+270):(29+270))'];
leftfingerRTs = [RT((31+90):(59+90)) RT((31+180):(59+180)) RT((31+270):(59+270))];
alternatingfingerRTs = [RT((61+90):(89+90)) RT((61+180):(89+180)) RT((61+270):(89+270))];

rightfingerVariability=std(rightfingerRTs)/mean(rightfingerRTs);
leftfingerVariability=std(leftfingerRTs)/mean(leftfingerRTs);
alternatingfingerVariability=std(alternatingfingerRTs)/mean(alternatingfingerRTs); 
std(rightfingerVariability)/mean(rightfingerVariability);

%remove outlier
alternatingDurationMeanOutliers=mean(rmoutliers(alternatingfingerRTs(:)));

stats = [rightDurationMean leftDurationMean alternatingDurationMean alternatingDurationMeanOutliers normAlternating rightfingerVariability leftfingerVariability alternatingfingerVariability]
groupData = [groupData; stats];

end

end