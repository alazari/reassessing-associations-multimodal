# Repository for code and materials from 'Reassessing associations between white matter and behaviour with multimodal microstructural imaging' by Lazari et al.

Full paper available here: https://www.biorxiv.org/content/10.1101/2020.12.15.422826v2

For any queries, you are welcome to get in touch with Alberto Lazari (alberto.lazari@ndcn.ox.ac.uk).
